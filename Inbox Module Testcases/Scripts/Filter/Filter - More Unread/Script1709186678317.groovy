import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r0'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (4)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (4)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/div_Home_MuiGrid-root css-1d2hfx (4)'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Inbox (4)'))

WebUI.click(findTestObject('Object Repository/Page_(313) Inbox/span_Skip this Short and Sweet Tour'))

WebUI.click(findTestObject('Object Repository/Page_(313) Inbox/button_Morearrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Page_(313) Inbox/input_Show messages that have not been open_fa21af'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(313) Inbox/input_Show messages that have not been open_fa21af'), 
    Keys.chord(Keys.ESCAPE))

WebUI.rightClick(findTestObject('Object Repository/Page_(313) Inbox/div_18'))

WebUI.verifyTextPresent('Mark Chat as Read', false)

WebUI.sendKeys(findTestObject('Object Repository/Page_(313) Inbox/li_markunreadMark Chat as Read'), Keys.chord(Keys.ESCAPE))

WebUI.click(findTestObject('Object Repository/Page_(313) Inbox/span_account_circle'))

WebUI.click(findTestObject('Object Repository/Page_(313) Inbox/span_Logout'))

WebUI.closeBrowser()

