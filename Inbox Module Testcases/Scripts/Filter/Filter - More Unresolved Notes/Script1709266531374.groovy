import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r0'), '9261505795')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (7)'), 'k3vh9FOVV1+Tg1rlITHUaA==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (7)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/div_Home_MuiGrid-root css-1d2hfx (7)'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Inbox (7)'))

WebUI.click(findTestObject('Object Repository/Page_(325) Inbox/span_Skip this Short and Sweet Tour'))

WebUI.click(findTestObject('Object Repository/Page_(325) Inbox/button_Morearrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Page_(325) Inbox/input_Show contacts which have unresolved n_8165f2'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(325) Inbox/input_Show contacts which have unresolved n_8165f2'), 
    Keys.chord(Keys.ESCAPE))

WebUI.click(findTestObject('Object Repository/Page_Inbox/span_Ivan (2)'))

WebUI.rightClick(findTestObject('Object Repository/Page_Inbox - Ivan/span_Ivan (3)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/li_portraitContact Info (2)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/svg_March 1, 2024 at 1109 AM_MuiSvgIcon-roo_56cc26'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Inbox - Ivan/li_Mark as resolved'), 'Mark as resolved')

WebUI.closeBrowser()

