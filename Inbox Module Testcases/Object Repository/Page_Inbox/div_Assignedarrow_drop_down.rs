<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Assignedarrow_drop_down</name>
   <tag></tag>
   <elementGuidId>695ef10e-e616-47a4-a1d1-d3af774f9f79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div/div[2]/div/button[3]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.MuiButtonBase-root.MuiButton-root.MuiButton-outlined.MuiButton-outlinedSecondary.MuiButton-sizeMedium.MuiButton-outlinedSizeMedium.MuiButton-colorSecondary.MuiButton-root.MuiButton-outlined.MuiButton-outlinedSecondary.MuiButton-sizeMedium.MuiButton-outlinedSizeMedium.MuiButton-colorSecondary.css-iptbkr > div.MuiGrid-root.css-1htn2r6</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ff8514c7-e1b0-4fb5-929d-d1883805cd78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-1htn2r6</value>
      <webElementGuid>57aa9e3e-ebbc-4ac9-85a4-4f5f2491ce8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assignedarrow_drop_down</value>
      <webElementGuid>c1acdc24-258d-407c-85c6-d8175fa2c88e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-1w6jn5e&quot;]/div[@class=&quot;MuiGrid-root tour-filters css-1r812nq&quot;]/div[@class=&quot;MuiGrid-root css-1lqzzmu&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedSecondary MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorSecondary MuiButton-root MuiButton-outlined MuiButton-outlinedSecondary MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorSecondary css-iptbkr&quot;]/div[@class=&quot;MuiGrid-root css-1htn2r6&quot;]</value>
      <webElementGuid>ef644058-0e50-4c3d-a1aa-b203363efb90</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div/div[2]/div/button[3]/div</value>
      <webElementGuid>8abae0e3-1c79-4d0a-addc-e2ef2f4adf40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_drop_down'])[2]/following::div[1]</value>
      <webElementGuid>2107694f-c307-408f-a905-5f02a462c59b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Assigned']/parent::*</value>
      <webElementGuid>9116ef1a-42a9-40d4-b435-c71ee19add5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]/div</value>
      <webElementGuid>fc8ec462-be63-485a-8a34-25e4bb87b675</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Assignedarrow_drop_down' or . = 'Assignedarrow_drop_down')]</value>
      <webElementGuid>86235003-384b-4111-a9f5-0004450ede66</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
