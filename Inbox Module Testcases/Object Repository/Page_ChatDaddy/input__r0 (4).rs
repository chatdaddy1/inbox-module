<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__r0 (4)</name>
   <tag></tag>
   <elementGuidId>f21372c2-c473-49dc-aafe-acf5c219fa62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=':r0:']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>10e54bb4-c131-4c40-b0a6-adf81540279a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0350ec69-3eb2-47f1-a7cc-3646afc7de61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>:r0:</value>
      <webElementGuid>1a349e5b-b199-401a-aa01-f088530c2398</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>49962a60-f886-4205-a819-22285e50b6b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputAdornedStart css-w2xudr</value>
      <webElementGuid>14619dc1-b589-4a46-8bba-a02cf87040e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>+63 9</value>
      <webElementGuid>ed9a885a-6918-4992-8001-ddb036a6b43f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;:r0:&quot;)</value>
      <webElementGuid>878710d4-a091-46ee-8295-116e505c03f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r0:']</value>
      <webElementGuid>a62b5074-6d83-49d2-a780-9f4b3ef102f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/div/div/div[2]/div/div/div/input</value>
      <webElementGuid>9dc44c9d-f926-41de-8ba5-034098285c73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>7039dec6-eaf5-41dd-b489-0629315cbf91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r0:' and @type = 'tel']</value>
      <webElementGuid>a53d2c50-d30d-4910-a00c-9aa864e5d808</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
