<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_markunreadMark Chat as Read</name>
   <tag></tag>
   <elementGuidId>b7b34c1c-7c66-491c-a985-168e8cd39876</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Mark Chat as Read' or . = 'Mark Chat as Read')]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='archive'])[1]/following::li[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>8ea469de-7704-42ba-9894-7024683361da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-lwzdbi</value>
      <webElementGuid>32e3c72a-278f-41ff-99bd-e9ce8e6442df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>ccd3169b-9899-4cd4-a4ea-185aa34af95c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>fab93c02-e26b-4782-b5b6-48540d7e5c33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mark Chat as Read</value>
      <webElementGuid>ff2949d2-88e0-42ce-9b1b-92e90263de37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiMenu-root MuiModal-root css-1sucic7&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper MuiMenu-paper MuiMenu-paper css-1kcgz0v&quot;]/ul[@class=&quot;MuiList-root MuiList-padding MuiMenu-list css-ribzw7&quot;]/li[@class=&quot;MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-lwzdbi&quot;]</value>
      <webElementGuid>4f3aeddb-95e6-40a5-bd42-43b30a702869</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='archive'])[1]/following::li[1]</value>
      <webElementGuid>9504fc58-8286-42eb-908b-f91e28507edc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Mark Chat as Read']/parent::*</value>
      <webElementGuid>33194916-0d8e-47cb-89c1-f17fee3adccb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]</value>
      <webElementGuid>41e6796b-d4af-4835-a871-6babd56a94d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'markunreadMark Chat as Read' or . = 'markunreadMark Chat as Read')]</value>
      <webElementGuid>6c93f506-d39d-4011-a14a-318dc99e39dc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
