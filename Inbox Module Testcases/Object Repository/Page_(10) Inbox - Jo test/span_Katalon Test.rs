<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Katalon Test</name>
   <tag></tag>
   <elementGuidId>d0b1c247-2def-4aa2-bbd1-a3f2eb6f672c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[2]/div/div[5]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b5aeefec-c01f-4148-9e21-22f6e20f8200</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallMedium css-p9ec3o</value>
      <webElementGuid>0bb22406-c160-421f-ae72-09b88932e6b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Katalon Test</value>
      <webElementGuid>5966ffec-3df0-4035-b306-6345bcf399bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Katalon Test: </value>
      <webElementGuid>ef7e82d7-0db5-492b-a121-ae7eb183cd72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-13tphjr&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root css-1fghw63&quot;]/div[@class=&quot;MuiGrid-root css-1eiz13t&quot;]/div[@class=&quot;MuiGrid-root css-7j22ch&quot;]/div[@class=&quot;MuiGrid-root css-k1la3o&quot;]/div[@class=&quot;MuiGrid-root css-v2cu41&quot;]/div[2]/div[2]/div[1]/div[@class=&quot;MuiGrid-root css-1bxei5x&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallMedium css-p9ec3o&quot;]</value>
      <webElementGuid>b6d08e73-a5d2-4bd7-9982-292df5012237</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div[2]/div/div[5]/span</value>
      <webElementGuid>6630b4ac-3b24-41a9-b845-17080ef14b99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[8]/following::span[1]</value>
      <webElementGuid>19eaf0e7-7b04-42ab-80e7-9ccbd4934df0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[9]/preceding::span[1]</value>
      <webElementGuid>b8a84a76-81c0-4181-a61e-fda344e202be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Katalon Test']/parent::*</value>
      <webElementGuid>085f9734-9992-4b49-b9e6-25b37724a742</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[5]/span</value>
      <webElementGuid>fe77f9c4-708b-4a07-9c57-7f0a5aec22bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'Katalon Test' and (text() = 'Katalon Test: ' or . = 'Katalon Test: ')]</value>
      <webElementGuid>9de14197-8d72-4c24-b280-ae8c17a6164a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
