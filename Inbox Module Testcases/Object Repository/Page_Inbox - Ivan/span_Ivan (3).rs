<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Ivan (3)</name>
   <tag></tag>
   <elementGuidId>496a2cfe-9386-4ed3-ae1d-bd8ac0d2dcba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div/div[2]/div[3]/div/div[2]/div/a/div[2]/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-baseMedium.css-1nchl2b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>33276d9b-b6fd-49a9-a0b7-8d8d9d557446</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-1nchl2b</value>
      <webElementGuid>1727d8ad-fd2d-4eb7-add5-5bc2a3d08d0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Ivan</value>
      <webElementGuid>c59d61a7-456a-46ab-8cf6-b858f5b2b121</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ivan</value>
      <webElementGuid>833c9320-f9e2-45c7-93e1-27cc9c65db1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/div[1]/div[2]/div[1]/a[@class=&quot;MuiGrid-root css-1ijopu2&quot;]/div[@class=&quot;MuiGrid-root css-1vlsgix&quot;]/div[@class=&quot;MuiGrid-root css-1mr0hd4&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1nchl2b&quot;]</value>
      <webElementGuid>b9f9d11a-ddb0-4462-95c0-dcdadd3224ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div/div[2]/div[3]/div/div[2]/div/a/div[2]/div/span</value>
      <webElementGuid>fccda6a4-71a7-4c92-ba68-7960803aec84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[2]/following::span[3]</value>
      <webElementGuid>93c218fc-6a95-4616-8689-73f0d3fbd593</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::span[4]</value>
      <webElementGuid>dd162714-e574-4ebd-9fb1-8424eb9a75ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done'])[1]/preceding::span[2]</value>
      <webElementGuid>de13c5cd-4041-42d6-896f-0580922abc01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test'])[1]/preceding::span[3]</value>
      <webElementGuid>29aee5b1-8497-4ae8-8618-e9eadb05a075</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/span</value>
      <webElementGuid>9513a630-303e-4ec9-bcde-5e81d0cb6e82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'Ivan' and (text() = 'Ivan' or . = 'Ivan')]</value>
      <webElementGuid>5f2aa8b0-7546-4a3c-bba1-cbea31adfc54</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
