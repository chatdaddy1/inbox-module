<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_portraitContact Info (3)</name>
   <tag></tag>
   <elementGuidId>40bc9982-b041-4d76-9c82-363975f1cbbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.MuiButtonBase-root.MuiMenuItem-root.MuiMenuItem-gutters.MuiMenuItem-root.MuiMenuItem-gutters.css-lwzdbi</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>3c94dd4d-7cab-441c-a8b1-cda2f2b20093</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-lwzdbi</value>
      <webElementGuid>f4e863bc-f420-464a-8247-8b9e4303377e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>91916ad4-dd38-4206-86bf-5f1e181e9740</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>95f8c163-3a30-4003-a49e-21b3323fc7be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>portraitContact Info</value>
      <webElementGuid>3faf4b2f-73b6-44e0-8894-26a6df36fc1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiMenu-root MuiModal-root css-1sucic7&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper MuiMenu-paper MuiMenu-paper css-1kcgz0v&quot;]/ul[@class=&quot;MuiList-root MuiList-padding MuiMenu-list css-ribzw7&quot;]/li[@class=&quot;MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-lwzdbi&quot;]</value>
      <webElementGuid>c82a31bb-1443-48c0-bd66-7aefae7cd4f1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::li[1]</value>
      <webElementGuid>19ffa2dd-3cd7-4d4c-a566-d65e5ddc3372</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='mic'])[1]/following::li[1]</value>
      <webElementGuid>b4a91ec6-f454-449a-826d-9f92a8ef7178</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Contact Info']/parent::*</value>
      <webElementGuid>35c05604-553f-4938-93b3-881bd924e4d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/li</value>
      <webElementGuid>5581a5b6-5d5c-40eb-b33d-83d98e1c30be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'portraitContact Info' or . = 'portraitContact Info')]</value>
      <webElementGuid>da29215c-4313-45f4-ad34-814382bdda77</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
