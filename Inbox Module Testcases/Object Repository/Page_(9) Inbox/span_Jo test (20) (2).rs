<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jo test (20) (2)</name>
   <tag></tag>
   <elementGuidId>e64d4dc0-c9ab-4928-b480-06b708d4d1a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a[2]/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0bd0613e-4ff3-4551-8ecc-9e3e46c7eb6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-1nchl2b</value>
      <webElementGuid>93246bac-f122-4098-99a6-69d37c7becb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>0e71b7b6-33b0-4c0e-b927-6d2c08b3e7a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>4629e382-249d-4b70-98aa-f15c28844999</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/div[1]/div[2]/div[1]/a[@class=&quot;MuiGrid-root css-128ea43&quot;]/div[@class=&quot;MuiGrid-root css-1vlsgix&quot;]/div[@class=&quot;MuiGrid-root css-1mr0hd4&quot;]/div[@class=&quot;MuiGrid-root css-1trzygj&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1nchl2b&quot;]</value>
      <webElementGuid>6e68cf9f-5e01-4982-a110-ddcd74d56866</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a[2]/div[2]/div/div/span</value>
      <webElementGuid>dc8da89e-bbb1-4647-b19d-ddd1735bc143</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[3]/following::span[3]</value>
      <webElementGuid>73b30ae5-daae-4a68-a5b3-9dfe54241dee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[1]/following::span[6]</value>
      <webElementGuid>cf082355-2ac6-4ca7-a165-029f7645f16a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Note'])[1]/preceding::span[2]</value>
      <webElementGuid>79cb25c5-bf4b-437c-ab77-3a14d68d3a23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VIP'])[1]/preceding::span[3]</value>
      <webElementGuid>d9a2b82b-f645-47b2-a191-c3126c903bdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jo test']/parent::*</value>
      <webElementGuid>2aa6ea86-cc5b-42f1-a322-8c0da84591c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]/div[2]/div/div/span</value>
      <webElementGuid>66bc4256-a82c-4945-ba7c-31d853a757fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'Jo test' and (text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>14944912-c649-4598-998f-f75d0162be9c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
