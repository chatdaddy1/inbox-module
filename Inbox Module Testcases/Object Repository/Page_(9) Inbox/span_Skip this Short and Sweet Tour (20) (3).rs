<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Skip this Short and Sweet Tour (20) (3)</name>
   <tag></tag>
   <elementGuidId>03f24ae3-a030-4f1e-a039-d92eca0cec3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallSemiBold.css-1ocob9</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>960e49be-488f-443e-8640-501cffbed84c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallSemiBold css-1ocob9</value>
      <webElementGuid>794fd903-add6-4961-8d7e-bd2716be870f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Skip this Short and Sweet Tour</value>
      <webElementGuid>7d97a6aa-f1cf-4d1e-878c-72ec0d59311b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-joyride-step-0&quot;)/div[@class=&quot;__floater __floater__open&quot;]/div[@class=&quot;__floater__body&quot;]/div[@class=&quot;MuiGrid-root css-grxqdy&quot;]/div[@class=&quot;MuiGrid-root css-b486k7&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-iotrky&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallSemiBold css-1ocob9&quot;]</value>
      <webElementGuid>37b56214-4c06-48b8-8f06-86327d73848f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>122cb33c-42f6-4c6c-b0df-30e2ff60f389</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Take the Tour'])[1]/following::span[2]</value>
      <webElementGuid>528a34d3-c549-4029-b4e7-d0574ae832dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='It’s going to take just a few seconds.'])[1]/following::span[3]</value>
      <webElementGuid>9c99d87e-6990-44f5-b0bf-1f9d28940485</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Skip this Short and Sweet Tour']/parent::*</value>
      <webElementGuid>3d525af8-a03b-4ca2-a9e0-06eb9977c6a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>0a6df911-883e-4ce4-9b75-bfea6053a84d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Skip this Short and Sweet Tour' or . = 'Skip this Short and Sweet Tour')]</value>
      <webElementGuid>c51d7c57-b0bc-49ce-9bb0-6de133a7ed3b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
