<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jo test (2)</name>
   <tag></tag>
   <elementGuidId>97f7670b-b7af-44ef-b9c6-8773382c348a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a[3]/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ea5a9eaa-199c-4230-9f9c-95e3968f58ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-1nchl2b</value>
      <webElementGuid>c0f82b7e-9a10-4af7-9b38-beb149f268d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>55e7de9c-3be1-4034-8875-8b4a993f3b6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>ef803c5f-4980-4493-bf53-788eace6be73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/div[1]/div[2]/div[1]/a[@class=&quot;MuiGrid-root css-128ea43&quot;]/div[@class=&quot;MuiGrid-root css-1vlsgix&quot;]/div[@class=&quot;MuiGrid-root css-1mr0hd4&quot;]/div[@class=&quot;MuiGrid-root css-1trzygj&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1nchl2b&quot;]</value>
      <webElementGuid>e7782199-e9a1-4ea3-b9dc-c49ff06a8de2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/div/div[2]/div/a[3]/div[2]/div/div/span</value>
      <webElementGuid>61a8fe14-1396-47ab-9442-09bd891e0afe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[4]/following::span[3]</value>
      <webElementGuid>fbedf5a7-0dd8-4110-a706-e077047e4c8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[2]/following::span[6]</value>
      <webElementGuid>db76148c-dc6f-4e28-9c3c-1fd42868dec7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[2]/preceding::span[2]</value>
      <webElementGuid>2d5e54b9-206f-424e-8bcb-89018a9b7036</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test152154165132151515'])[1]/preceding::span[3]</value>
      <webElementGuid>bbad2b60-4bf8-4864-b33a-51dfc8062c95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jo test']/parent::*</value>
      <webElementGuid>d8b19fea-2840-4bb3-a903-c620f6019d17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]/div[2]/div/div/span</value>
      <webElementGuid>804cc190-db4b-4e30-92f9-35afe060f812</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'Jo test' and (text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>ec93c5ac-7fa0-47e1-8396-7602c0e5a20b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
