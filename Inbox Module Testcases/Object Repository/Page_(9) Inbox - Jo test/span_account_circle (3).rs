<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_account_circle (3)</name>
   <tag></tag>
   <elementGuidId>1fb5f4b7-a78d-423b-8b1e-d2454b001dc3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div/div/div/div[2]/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-outlined.MuiBox-root.css-11ahsy5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5cd9f260-84fb-4e17-be33-c9b20c51db8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-11ahsy5</value>
      <webElementGuid>6e3ea199-2142-49a9-9ce2-766e54efd0bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>account_circle</value>
      <webElementGuid>8a6c92e9-2008-4557-b11a-f7ca18dec5c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiCollapse-root MuiCollapse-vertical MuiCollapse-entered css-c4sutr&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-vertical css-hboir5&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-vertical css-8atqhb&quot;]/div[@class=&quot;MuiGrid-root css-42de2x&quot;]/div[@class=&quot;MuiGrid-root css-1ymz3lr&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-1jwxc8t&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-11ahsy5&quot;]</value>
      <webElementGuid>1a45e8ca-85b0-4f60-9ee8-d83d96f1786e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>49c3bf59-ba7e-464e-bf65-88525a8947a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='help'])[1]/following::span[2]</value>
      <webElementGuid>294d358c-ad1d-4a5d-b0ea-1424ed52ba5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen'])[1]/following::span[4]</value>
      <webElementGuid>8314978a-4f57-400c-8469-bf1eecf9c751</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[1]/preceding::span[4]</value>
      <webElementGuid>6e1b9408-fe0a-4c10-8c4e-521bd0d9a50a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[1]/preceding::span[5]</value>
      <webElementGuid>0882f02e-f6a6-4263-8dc6-a2c73fd8a9d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='account_circle']/parent::*</value>
      <webElementGuid>e1544b98-e11f-4ded-b849-4f0f667278b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/span</value>
      <webElementGuid>1117d44f-95fa-4afa-8a9a-e4842417aa85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'account_circle' or . = 'account_circle')]</value>
      <webElementGuid>c12ed338-d91e-4d29-a0aa-408c1830d513</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
