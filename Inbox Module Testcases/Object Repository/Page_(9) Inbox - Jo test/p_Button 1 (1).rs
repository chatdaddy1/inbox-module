<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Button 1 (1)</name>
   <tag></tag>
   <elementGuidId>76017c93-38bc-4537-a90e-fd3e58d0f8b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFED79E6AD39B4|1-content']/div[2]/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b3dc6d5b-be13-4581-acc5-b0d3826674f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-egrk5c</value>
      <webElementGuid>b3b048d2-4b9b-4eea-a4c2-d0e655b7860f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Button 1</value>
      <webElementGuid>3590b3ce-f620-446f-a05d-02cf8047ce90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFED79E6AD39B4|1-content&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-rg2bq0&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-item css-12yvitw&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-egrk5c&quot;]</value>
      <webElementGuid>519cc39c-c388-4460-b863-31fe89559c70</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED79E6AD39B4|1-content']/div[2]/div/p</value>
      <webElementGuid>7f7cc348-b561-482c-b056-e72981437e1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test button'])[3]/following::p[1]</value>
      <webElementGuid>cb9a1915-1398-4ccf-b952-d5bab42a7d2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[21]/following::p[1]</value>
      <webElementGuid>5a9c9995-ce81-45c4-88b2-11eab76b7521</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jo Arao'])[14]/preceding::p[4]</value>
      <webElementGuid>1b809ba4-509d-48f6-b46c-359d7f4602e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 8, 2024 at 6:29 PM'])[1]/preceding::p[4]</value>
      <webElementGuid>e1c065f8-57c8-40cc-88b9-316f43618878</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div/div[2]/div/p</value>
      <webElementGuid>a7a76639-d072-4b03-ac00-ea6100dcff47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Button 1' or . = 'Button 1')]</value>
      <webElementGuid>77ca0679-ea55-40b1-a986-e4e12a043c41</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
