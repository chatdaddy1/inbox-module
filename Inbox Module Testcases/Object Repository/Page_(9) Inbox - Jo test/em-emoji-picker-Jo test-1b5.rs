<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>em-emoji-picker-Jo test-1b5</name>
   <tag></tag>
   <elementGuidId>6000c3bc-5440-4298-a880-ce15b946ca25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//em-emoji-picker</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>em-emoji-picker</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>em-emoji-picker</value>
      <webElementGuid>2e9ca05e-1fb0-4e05-b842-55503f7dce99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-rfalj3&quot;]/div[1]/em-emoji-picker[1]</value>
      <webElementGuid>5c736409-8c3b-4cbf-b18d-ebd199ca2161</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//em-emoji-picker</value>
      <webElementGuid>d52dbe84-9223-4ad2-8312-c4eba69d4255</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
