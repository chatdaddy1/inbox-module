<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_arrow_right (1) (1) (1)</name>
   <tag></tag>
   <elementGuidId>bcad04d5-43bc-4f4b-8ee9-d4a10a524728</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[4]/div/span/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-rounded.MuiBox-root.css-159niyq</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>44e5222f-06ea-426a-adee-998578df384a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-rounded MuiBox-root css-159niyq</value>
      <webElementGuid>c136e121-95df-4201-9839-a6e8d5e14b29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>arrow_right</value>
      <webElementGuid>42b78879-0fbf-4de8-9aed-d24a4db501c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-13tphjr&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root css-1fghw63&quot;]/div[@class=&quot;MuiGrid-root css-1eiz13t&quot;]/div[@class=&quot;MuiGrid-root css-7j22ch&quot;]/div[@class=&quot;MuiGrid-root css-1c8gsuk&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1oz3psz&quot;]/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-1cijpyl&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/span[@class=&quot;material-symbols-rounded MuiBox-root css-159niyq&quot;]</value>
      <webElementGuid>beb0004c-cf58-4671-84da-e04404532ef2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[4]/div/span/div/div/div/span</value>
      <webElementGuid>8b34ca36-6ed6-4ad8-aeab-14411d5ed479</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 9, 2024 at 3:32 PM'])[2]/following::span[3]</value>
      <webElementGuid>9ffccc4f-f34f-4f1c-b349-939799c28a4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='grid_view'])[1]/preceding::span[1]</value>
      <webElementGuid>04f64424-5111-4385-96ea-fd77caf370dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add'])[4]/preceding::span[2]</value>
      <webElementGuid>fce7195f-cf02-4ab2-9711-a17357b4aefe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/span/div/div/div/span</value>
      <webElementGuid>c94f7ddf-7d6a-47e9-99c7-6fe61b350017</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'arrow_right' or . = 'arrow_right')]</value>
      <webElementGuid>f414626b-1037-4c1f-9d29-12eb62f6c8d2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
