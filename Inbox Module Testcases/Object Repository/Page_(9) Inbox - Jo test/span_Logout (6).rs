<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Logout (6)</name>
   <tag></tag>
   <elementGuidId>27a05a36-5310-4fba-835b-e18b276ded16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='logout'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallRegular.css-1d2s0c2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b17148d4-200f-4ef0-872e-ce8687a65042</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallRegular css-1d2s0c2</value>
      <webElementGuid>91b1c26e-efa3-4ca8-bc71-82c13e07e1b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Logout</value>
      <webElementGuid>fa1aefd9-2a9f-4da7-8fe1-40ec5de6da27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation8 MuiPopover-paper css-6u8kux&quot;]/div[@class=&quot;MuiBox-root css-1r99qxv&quot;]/li[@class=&quot;MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-105266g&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallRegular css-1d2s0c2&quot;]</value>
      <webElementGuid>1bbf1d1a-b897-4c6b-9843-53cb5566f147</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='logout'])[1]/following::span[1]</value>
      <webElementGuid>4f1a7351-6097-4f17-aa60-1de9a949a022</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collect Locale URLS'])[1]/following::span[3]</value>
      <webElementGuid>1ab53c69-308a-445e-8559-b2df10f35ef6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Logout']/parent::*</value>
      <webElementGuid>249a1015-cd97-40c0-9087-419fc6b5450e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/span[2]</value>
      <webElementGuid>3269be5a-7b90-494b-9da0-32d9b838fb9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Logout' or . = 'Logout')]</value>
      <webElementGuid>52fa9616-bd65-400d-9e40-bc16509a8c33</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
