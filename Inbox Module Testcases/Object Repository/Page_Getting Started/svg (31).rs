<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (31)</name>
   <tag></tag>
   <elementGuidId>c3eff3fd-9cd9-4a2e-a6f1-9226d01f438c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][8]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiBox-root.css-0 > div.MuiGrid-root.css-wivtdb > div > svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>3f4d01bb-6100-45a7-a95e-0ba761633a68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>c30450c7-3318-43b7-960c-e82359c45693</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns:xlink</name>
      <type>Main</type>
      <value>http://www.w3.org/1999/xlink</value>
      <webElementGuid>53b90636-a45f-4776-b585-f57c53397c25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 500 500</value>
      <webElementGuid>ed7677d6-b711-4d4f-95fd-4f7fc6909625</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>e039959c-4fc6-4656-a6b7-59aee85c7e63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>46ad6812-569e-43a5-96f8-aac4105917d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>preserveAspectRatio</name>
      <type>Main</type>
      <value>xMidYMid meet</value>
      <webElementGuid>514f03a7-89d1-445a-8024-6712d74756d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-yhefzq&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/div[@class=&quot;MuiBox-root css-gxs633&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]/div[@class=&quot;MuiBox-root css-0&quot;]/div[@class=&quot;MuiGrid-root css-wivtdb&quot;]/div[1]/svg[1]</value>
      <webElementGuid>f0d3fae4-e430-46d5-bc5a-11fe663f94f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][8]</value>
      <webElementGuid>2eb05a63-e08e-476e-85c6-bfe3b28a31e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
