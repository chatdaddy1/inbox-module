<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Inbox (43) (1)</name>
   <tag></tag>
   <elementGuidId>054b5aaa-f0a5-4378-a672-8bc806f8ca73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Inbox'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-baseMedium.css-57mpad</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b2a14956-7298-475e-a8c6-6df744f962e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-57mpad</value>
      <webElementGuid>4134cda4-5d57-4a3a-8d91-0baa15e37bad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Inbox</value>
      <webElementGuid>9d10d3bc-bc31-4132-854e-81d38478b1fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;base-Popper-root css-b82va4&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-57mpad&quot;]</value>
      <webElementGuid>6c408b6b-c62c-4441-962b-e32c9a3813c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inbox'])[1]/following::span[1]</value>
      <webElementGuid>ad5458f8-057e-45cd-bcb6-883cfec6207b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_forward_ios'])[8]/following::span[3]</value>
      <webElementGuid>b35c0b8d-66be-485b-9c09-08c82ebf631d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invite Teammate'])[1]/preceding::span[1]</value>
      <webElementGuid>80810f43-0e82-42e0-8110-d7944f362bdf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Channels'])[1]/preceding::span[2]</value>
      <webElementGuid>3736b424-8f78-47b6-bc6a-59d5a241a8f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a/span</value>
      <webElementGuid>15395a16-91cb-4cbc-8a9b-dd988383f17d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Inbox' or . = 'Inbox')]</value>
      <webElementGuid>1aa0f1d4-0687-4bdd-9087-61501cdae48c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
