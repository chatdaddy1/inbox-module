<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_add (2)</name>
   <tag></tag>
   <elementGuidId>55d1da39-4fcb-49bb-9800-4e98f25e8a78</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[16]/following::span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiButton-icon.MuiButton-endIcon.MuiButton-iconSizeMedium.css-pt151d > span.material-symbols-outlined.MuiBox-root.css-14d0z92</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ad5596fd-390f-49ad-a652-fac5a4362652</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-14d0z92</value>
      <webElementGuid>3756829b-420f-489a-a193-da3330976576</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add</value>
      <webElementGuid>3c4bc75a-5678-4b5f-a448-3cd178300f9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-1qk7w6v&quot;]/div[2]/div[2]/div[1]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-14vzcio&quot;]/span[@class=&quot;MuiButton-icon MuiButton-endIcon MuiButton-iconSizeMedium css-pt151d&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-14d0z92&quot;]</value>
      <webElementGuid>53810169-9df0-44df-a854-e889321b3671</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[16]/following::span[2]</value>
      <webElementGuid>a0f34af2-38c8-47ae-8e44-e08e947cbf0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/button/span/span</value>
      <webElementGuid>8c98d803-dc2b-4d87-b2c4-9ee4c80bb868</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'add' or . = 'add')]</value>
      <webElementGuid>306df213-c46c-424c-ac28-d50067746ce4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
