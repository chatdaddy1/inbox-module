<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_searchcancellabelAdd Tag1</name>
   <tag></tag>
   <elementGuidId>6e0d1528-6d29-43ff-8da7-210254c0f7da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[5]/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiPaper-root.MuiPaper-25.MuiPaper-rounded.MuiPopover-paper.css-1qk7w6v</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '​searchcancel​labelAdd Tag1' or . = '​searchcancel​labelAdd Tag1')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a7315150-502b-44c4-8954-2c0f474e5117</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-1qk7w6v</value>
      <webElementGuid>fd818423-660f-4867-8553-ece833aa5de7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>c53e353b-82a0-4f6e-8e17-71f8b43418b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>​searchcancel​labelAdd Tag1</value>
      <webElementGuid>3154572d-d2d2-477c-a540-0980c70cb254</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-1qk7w6v&quot;]</value>
      <webElementGuid>0e76bb5d-aed6-4027-ba66-c7686823e4ac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partner'])[1]/following::div[15]</value>
      <webElementGuid>235a3583-91e6-4016-a4d5-b9aae3f12a7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscription End Date:'])[1]/following::div[16]</value>
      <webElementGuid>4397a660-fcf6-4f28-8bd0-8aea435f8d42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[3]</value>
      <webElementGuid>34da928e-3cf5-4339-a803-9882773cce95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '​searchcancel​labelAdd Tag1' or . = '​searchcancel​labelAdd Tag1')]</value>
      <webElementGuid>217b3246-4aff-49fc-88e4-f0d1e8c09646</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
